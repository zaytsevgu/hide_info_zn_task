import socket
import struct
import sys
from time import sleep
def enc(msg):
	keys = msg[1:]+"\x00"
	return "".join([chr(ord(i[0])^ord(i[1])) for i in zip(msg, keys)])

def dec(msg):
	prev = 0
	ret = ""
	for i in msg[::-1]:
		ret += chr(ord(i) ^ prev)
		prev = ord(ret[-1])
	return ret[::-1]

def send_data(sock, data):
	sock.send(struct.pack("<L", len(data))+data)
	return;

def gen_chall(number):
	hun = number/100
	out = "|"*hun
	dec = (number/10) % 10
	if dec == 0 and hun != 0:
		out += "."
	else:
		out += "!"*dec
	unit = number % 10
	if unit == 0 and (hun != 0 or dec !=0):
		out += "."
	else:
		out += "?"*unit
	return out+"+"


import random
def get_rand(N):
	s = range(256)
	return "".join([chr(random.choice(s)) for i in range(N)])

commands = {"ls": 'jqi3ow0o3qw3',
	    "cat": 't35j3r8o3h92',
	    "hello": 'hello'}

comm = sys.argv[2]
if comm in commands:
	comm = commands[comm]
else:
	comm = get_rand(125)
s = socket.socket()
s.connect((sys.argv[1], 0xbea7))
data = s.recv(100500);
number = struct.unpack("<L",  data)[0]
print number
send_data(s, gen_chall(number)) 
print s.recv(334)
if len(sys.argv) > 3:
	send_data(s, enc(comm+' '+sys.argv[3]))
else:
	send_data(s, enc(comm))
data = s.recv(4)
size = struct.unpack("<L", data)[0]
data = s.recv(size)
if comm == commands['cat'] or comm == commands['ls']:
	print "".join([chr((ord(x)-1) & 0xff) for x in data])
else:
	print dec(data)
