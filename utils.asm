global load_info
global process_nam
%include "vm.mac"

section .data
 sig   db "D3M0"
       dd 0
              ; save_ptr_to_our_mem
 prog1 dq retaddr
	load_val r10, local_stor
	load_val r8, call_stack
	save_reg_to_mem out_ptr, rdi
	save_reg_to_mem var1, rsi
	save_reg_to_mem var2, rdx
	place_val_to_reg var1, rbx 
	save_mem_mem arg1, out_ptr
	save_mem_mem arg2, var1	
	callFunc check_hdr
	branchZ result_call, prog1.fail
	get_val_to_rax out_ptr
	load_val rbx, 12
	dq add_rax_rbx
	save_reg_to_mem arg1, rax
	save_mem_mem arg2, var2
	sub_imm var1, 12 ,var1
	save_mem_mem arg3, var1
	callFunc load_program
	get_val_to_rax result_call
	save_reg_to_mem result_call, rax
	dq finish
    prog1.fail:
	place_imm_to_mem result_call, -1
	dq finish
	
  check_hdr:
	place_imm_to_mem result_call, 0
	get_val_to_rax arg1
	dq deref
	save_reg_to_mem local1, rax
	compare_equal arg2, local1, local2
	branchZ local2, check_hdr_nxt
	branch check_hdr_ret
   check_hdr_nxt:
	get_struct_shift arg1, 8
	save_reg_to_mem arg1, rax
	place_imm_to_mem arg2, 4
	place_imm_to_mem arg3, sig
	callFunc strcmp ;1 if equal
   check_hdr_ret:
	Restore
	dq finish
	
  load_program:
	;arg1 - ptr to program tables start
	;arg2 - ptr to output memory
	;arg3 - size mem
	place_imm_to_mem result_call, 0
	take_dword_offset_imm arg1, 0
	save_reg_to_mem local1, rax
	save_mem_mem local3, local1
	save_mem_mem local6, arg1
	save_mem_mem local7, arg2
	save_mem_mem local8, arg3
     lp.iter:
        compare_equal local1, local8, local2
	branchZ local2, lp.ok
	compare_less local8, local1, local2
	branchZ local2, lp.ok
	branch lp.ret
     lp.ok:
	save_mem_mem arg1, local7
	save_mem_mem arg2, local3
	save_mem_mem arg3, local6
	;callFunc strcpy
	dq mystrcpy
	addvar local7, local3, local7
	addvar local6, local3, local6
	add_imm result_call, 1, result_call
	compare_equal local1, local8, local2
	branchZ local2, lp.ret
	take_dword_offset_imm local6, 0
	save_reg_to_mem local3, rax
	addvar local1, local3, local1
	branch lp.iter
    lp.ret:
	Restore

  strcmp:
	load_val rax, 0
	save_reg_to_mem local1, rax
   strcmp_loop:
	compare_equal local1, arg2, local2
	branchZ local2, strcmp_ok
	take_byte_offset arg1, local1
	save_reg_to_mem local3, rax
	take_byte_offset arg3, local1
	save_reg_to_mem local4, rax
	compare_equal local3, local4, local2
	branchZ local2, strcmp_next_iter
	branch strcmp_fail
  strcmp_next_iter:
  	add_imm local1, 1, local1
	branch strcmp_loop
  strcmp_ok:
	load_val rax,1
	save_reg_to_mem result_call, rax
  strcmp_fail:	
	Restore
	dq finish


  strcpy:
	;arg1 - dest
	;arg2 - size
	;arg3 - source
	load_val rax,0
	save_reg_to_mem local1, rax
    strcpy_loop:
	compare_equal local1, arg2, local2
	branchZ local2, strcpy_ret
	take_byte_offset arg3, local1
 	save_byte_offset arg1, local1
	add_imm local1, 1, local1
	branch strcpy_loop
    strcpy_ret:
	Restore
	dq finish
	
  strstr:
	;arg1 - haystack
	;arg2 - size
	;arg3 - needle
	;arg4 - size
	place_imm_to_mem result_call, -1
	place_imm_to_mem local1, 0
	place_imm_to_mem local8, 0
	compare_equal arg4, local1, local2
	branchZ local2, strstr.ret_zero	
	compare_equal arg4, arg2, local2
	branchZ local2, strstr_loop
	compare_less arg4, arg2, local2
	branchZ local2, strstr_ret
	subvar arg2, arg4, local7
     strstr_loop:	
	dq mystrstr
	save_reg_to_mem result_call, rax
	branch strstr_ret
	take_byte_offset arg1, local1
	save_reg_to_mem local3, rax
	take_byte_offset arg3, local8
	save_reg_to_mem local4, rax
	compare_equal local3, local4, local2
	branchZ local2, strstr_loop_inner
	branch strstr_nextiter
	
	   strstr_loop_inner:
		save_mem_mem local4, local1
		add_imm local4, 1, local4
		place_imm_to_mem local3, 1
	   strstr_loop_inner1:
		compare_equal local3, arg4, local2
		branchZ local2, strstr_find
	  	take_byte_offset arg1, local4
		save_reg_to_mem local5, rax
		take_byte_offset arg3, local3
		save_reg_to_mem local6, rax
		compare_equal local5, local6, local2
		branchZ local2, strstr_loop_inner_nxt
		branch strstr_nextiter
	   strstr_loop_inner_nxt:
		add_imm local3, 1, local3
		add_imm local4, 1, local4
		branch strstr_loop_inner1
	   strstr_find:
		save_mem_mem result_call, local1
		branch strstr_ret

     strstr_nextiter:
	compare_equal local1, local7, local2
	branchZ local2, strstr_ret
	add_imm local1, 1, local1
	branch strstr_loop
     strstr.ret_zero:
	place_imm_to_mem result_call, 0
     strstr_ret:
	Restore
	dq finish


   NAM:
	load_val r10, local_stor
	load_val r8, call_stack
	save_reg_to_mem arg1, rdi
	save_reg_to_mem var1, rsi
	take_dword_offset_imm var1, 0
	save_reg_to_mem arg3, rax
	add_imm var1, 4, var1
	save_mem_mem arg2, var1
	save_reg_to_mem arg4, rdx
	callFunc2 NAM_process_iter
	place_imm_to_mem local1, 0
	eq_branch result_call, local1, NM.stp
	sub_imm var1, 4, var1
	get_val_to_rax arg6
	save_dword_offset_imm var1, 0
	branch NM.end
    NM.stp:
	place_imm_to_mem result_call, 2
    NM.end:
	dq finish

   NAM_process_iter:
;==================
;Nam table format
; 0: size
; 4: entries
; 
; entry format:
;    0: replaced size
;    4: replaced body
;    n: substitute size
;    n+4: substitute flags
;    n+8: substitute size
;
;===================


	;arg1 - NAM
	;arg2 - input
	;arg3 - input size
	;arg4 - temp_buffer
	;returns new length in arg6
	;place_imm_to_mem local1, 4
	place_imm_to_mem result_call, 0
	place_imm_to_mem local8, 0
	save_mem_mem local5, arg1
	dq dbg1
	save_mem_mem arg1, arg2
;	place_imm_to_mem arg6, -1
	take_dword_place local5, arg5
	save_mem_mem local1, local5
	fast_add local5, 4
	dq dbg1
	save_mem_mem local4, arg4
	addvar_fast local1, arg5
	save_mem_mem local2, arg3
    Npi.iter:
	dq magnificent_branch
		dq strstr_prep
		dq mystrstr
		save_rax_to_mem result_call
;		place_imm_to_mem result_call, -1
		dq magnificent_branch2
		save_mem_mem local7, result_call
		save_mem_mem local1, arg1
			subvar local2, result_call, local2
			subvar local2, local3, local2

			save_mem_mem arg3, arg1
			save_mem_mem arg1, local4
			save_mem_mem arg2, result_call
			dq dbg1
			dq mystrcpy

			addvar arg1, arg2, arg1
			addvar arg3, local3, local8
			addvar local8, arg2, local8

			take_dword_place local5, arg2
			fast_add local5, 4
			take_dword_place local5, local6
			fast_add local5, 4
			save_mem_mem arg3, local5
			dq dbg1
			dq mystrcpy
			addvar arg1, arg2, arg1
			;inc len out string
			addvar local7, arg2, local7
			save_mem_mem arg3, local8
			save_mem_mem arg2, local2
			dq dbg1
			dq mystrcpy
			addvar local7, arg2, local7
			save_mem_mem local8, local6
		save_mem_mem arg1, local1
		branch Npi.ret
    Npi.skip:
		dq string_skip
		dq magnificent_branch3
		branch Npi.iter

    Npi.ret:
	place_imm_to_mem local1, 0
	place_imm_to_mem result_call, 0
	eq_branch local1, local8, Npi.end
	save_mem_mem arg2, local7
	save_mem_mem arg3, local4
	dq mystrcpy
	save_mem_mem arg6, local7
	save_mem_mem result_call, local8
    Npi.end:
	Restore2
	dq finish


section .bss
	call_stack resq 256
	local_stor resq 4096
	save_stack resq 1
        out_ptr resq 1
        result_call resq 1
	var1 resq 1
	var2 resq 1
	arg1 resq 1
	arg2 resq 1
	arg3 resq 1
	arg4 resq 1
	arg5 resq 1
	arg6 resq 1
	arg7 resq 1
	local1 resq 1
	local2 resq 1
	local3 resq 1
	local4 resq 1
	local5 resq 1
	local6 resq 1
	local7 resq 1
	local8 resq 1
	res1 resq 1
	res2 resq 1
	cmp_res resq 2


section .text
magnificent_branch3:
	mov rsp, Npi.iter
	ret
magnificent_branch:
	mov rax, [local1]
	mov rbx, [local5]
	cmp rax, rbx
	jz mag_tt
	ret
  mag_tt:
	mov rsp, Npi.ret
	ret
magnificent_branch2:
	mov rax, [result_call]
	mov rbx, -1
	cmp rax, rbx
	jz mm2_tt
	ret
  mm2_tt:
	mov rsp, Npi.skip
	ret


dbg1:
	ret
dbg2:
	ret
dbg3:
	ret

pop_r10:
	pop r10
	ret
pop_r8:
	pop r8
	ret
mov_rax_r8:
	mov rax,r8
	ret

add_mem_imm_8:
	add qword [rax], 8
	ret

add_vars:
	mov rbx, [rbx]
	add [rax], rbx
	ret

add_mem_imm_4:
	add qword [rax], 4
	ret

mov_r9_rax:
	mov r9, rax
	ret

mov_rax_r9:
	mov rax, r9
	ret

add_rax_4:
	add rax,4
	ret
add_rax_8:
	add rax,8
	ret
add_rax_1:
	inc rax
	ret

get_eflags:
	lahf
	ret
mul_rax_rbx:
	mul rbx
	ret

xor_rax_rbx:
	xor rax, rbx
	ret

save_r8_mem:
	mov [r8], rax
	ret
take_r8_mem:
	mov rax, [r8]
	ret
add_rsp:
	add rsp, rax
	ret
change_stack:
	mov rsp, rax
	ret

take_r10_mem:
	mov rax, [r10]
	ret
save_r10_mem:
	mov [r10], rax
	ret

mov_r10_rax:
	mov r10, rax
	ret
mov_rax_r10:
	mov rax, r10
	ret
load_info:
        savesreg
	mov [save_stack], rsp
	mov rsp, prog1
	ret
process_nam:
	savesreg
	mov [save_stack], rsp
	mov rsp, NAM
	ret
 finish:
	mov rsp, [save_stack]
        restorereg
        mov rax, [result_call]
	ret
 mov_r8_rax:
	mov r8, rax
	ret
 mov_r9_mem_byte:
	mov [r9], al
	ret
 mov_r9_mem_dword:
	mov [r9], eax
	ret
 mov_r9_mem:	
	mov [r9], rax
	ret
 pop_r12:
        pop r12
 retaddr:
	ret
 pop_rax:
	pop rax;
	ret
 mov_rbx_rax:
	mov rbx, rax
	ret 
 inc_r10:
	add r10, 8
	ret
 dec_r10:
	sub r10, 8
	ret
 save_rax_r12:
	mov [r12], rax
	ret
 mov_rax_rdi:
	mov rax, rdi
	ret
 extractZ:
	shr rax, 6
	and rax, 1
	xor rax, 1
	ret
 jpz:
	mov rdx, [cmp_res]
	cmp rdx, rbx
	jz mygin
	ret
  mygin:
	mov rsp, rax
	ret
	
 pop_rbx:
	pop rbx
	ret
 mov_rax_rsi:
	mov rax, rsi
	ret
 deref:
	mov rax, [rax]
	ret
 strstr_prep:
	mov rax, [local2]
	mov [arg2], rax
	mov rax, [local5]
	mov rax, [rax]
	mov rbx, 0xffffffff
	and rax, rbx
	mov rbx, rax
	mov [local3], rax
	mov [arg4], rax
	mov rax, [local5]
	add rax,4
	mov [arg3], rax
	add rax, rbx
	mov [local5], rax
	ret

 string_skip:
	mov rax, [local5]
	mov rax, [rax]
	add rax, 8
	mov rbx, 0xffffffff
	and rax, rbx
	add qword [local5], rax
	ret

 deref_dword:
	mov rax, [rax]
	mov rbx, 0xffffffff
	and rax, rbx
	ret
 mov_rax_rax:
        ret

 and_rax_rbx:
	and rax,rbx
	ret
 sub_rax_rbx:
	sub rax, rbx
	ret
 pop_rdi:
	pop rdi
	ret
 pop_rsi:
	pop rsi
	ret
 add_rax_rbx:
	add rax, rbx
	ret
 mov_r12data_rax:
	mov rax, [r12]
	ret
 mov_rax_rdx:
	mov rax, rdx
	ret

call_rax:
	call rax
	ret

mystrcpy:
	mov rcx, [arg2]
	mov rdi, [arg1]
	mov rsi, [arg3]
	test rcx, rcx
	mov rax, 1
	and rax,rcx
	test rax,rax
	jnz copyb
	shr rcx, 1	
	rep movsw
	ret
    copyb:
	rep movsb
	ret

copy_val:
	movsq
	ret

mystrstr:
	;arg1 - haystack
	;arg2 - size
	;arg3 - needle
	;arg4 - size
	mov rdi, [arg1]
	mov rbx, [arg2]
	mov rsi, [arg3]
	mov r13, [arg4]
	;zero length needle
		test r13, r13
		jz .RET3
	mov rbp, rsi
	;skil if needle bigger than we
		cmp rbx, r13
		jl .RET
	;rbx - ptr to haystack[arg2-arg4]
		add rbx, rdi
		sub rbx, r13
	
	lea rdx, [rdi-2]
	;get number of dwords
		shr r13, 1
    .L1:
	;if we reached last - ret
	cmp rdx, rbx
	je .RET
	
	inc rdx
	inc rdx
	
	mov rdi, rdx
	mov rcx, rbx
	sub rcx, rdi
	mov ax, [rbp]
	shr rcx, 1
	add rcx, 1
	test rcx, rcx
	jnz .l3
	;add rdi, 2
	;jmp .l4
     .l3:	
	repne scasw
	jne .RET
	test rcx, rcx
	jne .kk
      .l4:
	mov dx, [rdi-2]
	cmp ax, dx
	jne .RET
     .kk:
	sub rdi, 2
	mov rdx, rdi
	mov rdi, rdx
	mov rsi, rbp
	mov rcx, r13
	repe cmpsw
	jne .L1 
	mov rax, rdx
	mov rdi, [arg1]
	sub rax, rdi
	ret
     .RET3:
	mov rax, 0
	ret
     .RET:
	mov rax, -1
	ret
	
