def dec(enc):
    t = enc[0]
    res = []
    for i in enc[1:]:
        res.append(chr(ord(t) ^ ord(i)))
        t = i
    res.append(enc[-1])
    return res

def enc(msg):
    t = msg[-1]
    kek = msg[::-1][1:]
    res = []
    for i in kek:
        res.append(chr(ord(t) ^ ord(i)))
        t = chr(ord(t) ^ ord(i))
    return res[::-1]
