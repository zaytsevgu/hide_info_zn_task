#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <dirent.h>
#include <string.h>
#include <signal.h>

#define ALARM_DELTA 1

typedef struct {
    unsigned int size;
    void* data;
} fdata;

void fdata_free(fdata* tofree) {
    free(tofree->data);
    free(tofree);
}

fdata* cat_file(char* path) {
    fdata* content = (fdata*)malloc(sizeof(fdata));
    if (content == NULL) {
        puts("Error due allocating fdata* in `cat_file`");
        return NULL;
    }

    FILE* file = fopen(path, "r");
    if (file == NULL) {
        puts("Error due opening file in `cat_file`");
        return NULL;
    }
    
    fseek(file, 0, SEEK_END);
    content->size = ftell(file);
    rewind(file);
    
    content->data = malloc(content->size);
    if (content->data == NULL) {
        puts("Error due allocationg fdata->data in `cat_file`");
        free(content);
        fclose(file);
        return NULL;
    }
    
    //may be we should to check numbers of readed bytes, better ask @groke
    fread(content->data, 1, content->size, file);
    fclose(file);

    return content;
}

int write_file(char* path, char* buf) {
    FILE* file = fopen(path, "a");
    if (file == NULL) {
        return NULL;
    }

    fwrite(buf, 1, strlen(buf) + 1, file);
    fclose(file);
     
    return 1;
}
char* list_dir(char* path) {
    struct dirent* dfile;
    DIR* dirp = opendir(path);
    if (dirp == NULL) {
        puts("Error due open directory in `list_dir`");
        return NULL;
    }
    
    unsigned int files_n = 0;
    unsigned int total_size = 0;
    char* result;

    while ((dfile = readdir(dirp)) != NULL) {
        if (strcmp(".", dfile->d_name) && strcmp("..", dfile->d_name)) {
            total_size += strlen(dfile->d_name);
            files_n++;
        }
    }
    rewinddir(dirp);
    result = (char*)malloc(total_size + files_n + 1);
	memset(result, '\0', total_size + files_n + 1);

    while ((dfile = readdir(dirp)) != NULL) {
        if (strcmp(".", dfile->d_name) && strcmp("..", dfile->d_name)) {
            strcat(result, dfile->d_name);
            result[strlen(result)] = '\n';

        }
    }
    result[total_size + files_n] = '\0';
    return result;
}

static void alarm_handler(int sig) {
	signal(SIGALRM, alarm_handler);
	alarm(ALARM_DELTA);
}

void set_alarm_antidbg() {
	signal(SIGALRM, alarm_handler);
}
