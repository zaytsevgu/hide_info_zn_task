

utils.o: utils.asm vm.mac
	nasm -felf64 utils.asm

mmm2017: main.c utils.o modwriter.py functions.c
	gcc -fstack-protector -o  mmm2017 main.c utils.o functions.c
	execstack -c mmm2017
	strip mmm2017
	python modwriter.py mmm2017
