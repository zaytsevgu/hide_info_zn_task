import sys
import struct

class Entry:
	def __init__(self, what, why, is_end):
		self.what = what
		self.why = why
		self.is_end = is_end

	def process(self):
		return struct.pack("<L", len(self.what))+\
			   self.what+struct.pack("<L", len(self.why))+\
			   struct.pack("<L", self.is_end)+self.why


class Table:
	def __init__(self, entries):
		self.entries = entries

	def process(self):
		out = ""
		for i in self.entries:
			out += i.process()
		return struct.pack("<L", len(out)+4)+out

import string
#alph = [x+"\x00" for x in string.printable]
alph = [chr(x)+"\x00" for x in range(1,256)]
#alph = ["e\x00"]
alph = [x+"\x00" for x in 'abcdefghijklmnopqrstuvwxyz1234567890']

alph2 = [chr(x)+"\x00" for x in range(0,256)]

def gen_table1():
	spec_start = '!!'
	spec_i = '??'
	delim = '||'
	entries = []
#	tbl = Table(entries)
#	return tbl.process()
	
	for i in alph:
		entries.append(Entry('{}{}{}{}'.format(delim, spec_i, i, i), '{}'.format(delim), 1))	
	entries.append(Entry('{}{}{}'.format(spec_start, spec_i, delim), '', 2))
	a = Table(entries)
	return a.process()

def gen_table2():	
	spec_start = '!!'
	spec_i = '??'
	delim = '||'
	entries = []
	for i in alph:
		entries.append(Entry('{}{}{}'.format(spec_i, i, delim),'{}{}{}'.format(delim, spec_i, i), 2))	
	for i in alph:
		for j in alph:
			entries.append(Entry('{}{}{}'.format(spec_i, i, j),'{}{}{}'.format(j, spec_i, i), 1))
	a = Table(entries)
	return a.process()

def gen_table3():	
	spec_start = '!!'
	spec_i = '??'
	delim = '||'
	entries = []
	for i in alph:
		entries.append(Entry('{}{}{}'.format(spec_i, i, delim),'{}{}{}'.format(delim, spec_i, i), 1))	
	a = Table(entries)
	return a.process()
	
def gen_table4():	
	spec_start = '!!'
	spec_i = '??'
	delim = '||'
	entries = []
	entries.append(Entry('{}{}{}'.format(spec_start, spec_i, delim),'', 2))
	entries.append(Entry('{}{}'.format(spec_start, delim), '', 2))
	entries.append(Entry('{}'.format(spec_i), '', 2))
	a = Table(entries)
	return a.process()

def gen_table5():
	spec_start = '!!'
	spec_i = '??'
	delim = '||'
	entries = []
	entries.append(Entry('{}'.format(spec_start),'{}{}'.format(spec_start, spec_i), 1))
	a = Table(entries)
	return a.process()

def gen_table6():
	spec_start = '!!'
	spec_i = '??'
	delim = '||'
	entries = []
	entries.append(Entry('{}'.format(spec_start),'{}{}'.format(spec_start, spec_i), 1))
	entries.append(Entry('','{}'.format(spec_start),1))
	a = Table(entries)
	return a.process()

def gen_table7():
    spec_i = '??'
    entries = [] 
    for i in alph2:
        for j in alph2:
            a = struct.unpack('<h', i)[0]
            b = struct.unpack('<h', j)[0]
            entries.append(Entry('{}{}{}'.format(spec_i, i, j), '{}{}{}'.format(chr(a^b)+'\x00', spec_i, j), 1))
    entries.append(Entry('{}'.format(spec_i), '', 2))
    entries.append(Entry('', '{}'.format(spec_i), 1)) 
    a = Table(entries)
    return a.process()

def gen_table9():
    spec_1 = '??'
    spec_2 = '||'
    entries = []
    for i in alph2:
        for j in alph2:
            a = struct.unpack('<h', i)[0]
            b = struct.unpack('<h', j)[0]
            entries.append(Entry('{}{}{}'.format(i, spec_1, j), '{}{}{}'.format(spec_1, chr(a^b)+'\x00', j), 1))
    for i in alph2:
	entries.append(Entry('{}{}'.format(i, spec_1), '{}{}'.format(spec_1, i), 1))
    entries.append(Entry('{}'.format(spec_1), '', 2))
    a = Table(entries)
    return a.process()

def gen_table10():
	spec_1 = '??'
	spec_2 = '||'
	entries = []
	for i in alph2:
		entries.append(Entry('{}{}'.format(spec_1, i), '{}{}'.format(i, spec_1), 1))
	
	entries.append(Entry('{}'.format(spec_1), '{}'.format(spec_1), 2));
	entries.append(Entry('', '{}'.format(spec_1), 1));
 	a = Table(entries)
	return a.process()	

def gen_table8():
    hun = '|\x00'
    dec = '!\x00'
    unit = '?\x00'
    
    entries = []
    entries.append(Entry('.\x00', '0\x00', 1))
    entries.append(Entry(hun * 10, '1\x003\x003\x007\x00', 1))
    for i in xrange(9, 0, -1):
        entries.append(Entry(hun * i, str(i)+'\x00', 1))
    entries.append(Entry(dec * 10, '1\x003\x003\x007\x00', 1))
    for i in xrange(9, 0, -1):
        entries.append(Entry(dec * i, str(i)+'\x00', 1))
    entries.append(Entry(unit * 10, '1\x003\x003\x007\x00', 1))
    for i in xrange(9, 0, -1):
        entries.append(Entry(unit * i, str(i)+'\x00', 1))
     
    entries.append(Entry('+\00', '', 2))
    a = Table(entries)
    return a.process()




if __name__ == '__main__':
	magic = struct.pack("<L", 0x3ea3b000)
	hdr = "D3M0"
	data = hdr + gen_table1()+gen_table2()+gen_table3()+gen_table4()+gen_table5()+gen_table6()+gen_table7()
	data += gen_table8()+gen_table9()+gen_table10()
	mod_size = struct.pack("<L", len(data)+8)
	mod_size2 = struct.pack("<Q", len(data)+8)
	f = open(sys.argv[1], "ab")
	fa = open('log.hd', 'wb')
	f.write(mod_size2+data+mod_size+magic)
	fa.write(mod_size2+data+mod_size+magic)
	fa.close()
	f.close()
